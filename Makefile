.PHONY: help
.DEFAULT_GOAL := help

define PRINT_HELP_PYSCRIPT
import re, sys

print("{ln}{sp}HELP{sp}{ln}".format(ln=24*"=", sp=5*" "))
for line in sys.stdin:
	category_match = re.match(r'^### (.*)$$', line)
	target_match = re.match(r'^([a-zA-Z0-9_-]+):.*?## (.*)$$', line)
	if category_match:
		category, = category_match.groups()
		print("\n{}:".format(category))
	if target_match:
		target, help = target_match.groups()
		print("  {:26} {}".format(target, help))
endef
export PRINT_HELP_PYSCRIPT

help:
	@python -c "$$PRINT_HELP_PYSCRIPT" < $(MAKEFILE_LIST)

### Docker commands
build: ## Build docker image
	docker build -t alert-scraper .

scrap: ## RUn scraper in docker
	docker run -v `pwd`:/app -it alert-scraper scrapy crawl iosco

### Localhost commands
local-scrap: ## Scrap iosco list
	scrapy crawl iosco

run-aws:
	aws ecs run-task \
		--task-definition arn:aws:ecs:eu-central-1:776112957202:task-definition/scamblock-pl-Scrapy-GB02F4DX7XB1-Scrapy \
		--launch-type FARGATE \
		--cluster scamblock-pl-Scrapy-GB02F4DX7XB1-ECSCluster-FjY9b8WI3cDv \
		--network-configuration "awsvpcConfiguration={subnets=[subnet-3ff84455,subnet-f0ed068c],securityGroups=[sg-007c9fbc706f5efbf],assignPublicIp=ENABLED}"
