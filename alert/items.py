# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class IoscoItem(scrapy.Item):
    company = scrapy.Field()
    regulator = scrapy.Field()
    regulator_url = scrapy.Field()
    jurisdiction = scrapy.Field()
    date = scrapy.Field()
    subject = scrapy.Field()
    comments = scrapy.Field()

class IoscoAdvancedItem(scrapy.Item):
    iosco_item_id = scrapy.Field()
    url = scrapy.Field()
