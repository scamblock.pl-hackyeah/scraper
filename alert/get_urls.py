import os
import psycopg2
from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings

from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging

def run_url_from_regulator_scrapers():
    process = CrawlerProcess(get_project_settings())

    hostname = os.environ.get('DB_HOST')
    username = os.environ.get('DB_USER')
    password = os.environ.get('DB_PASSWORD')
    database = os.environ.get('DB_DATABASE')

    connection = psycopg2.connect(
        host=hostname, user=username, password=password, dbname=database)
    cur = connection.cursor()
    cur.execute('SELECT id, regulator, regulator_url FROM "ioscoItems"')

    rows = cur.fetchall()
    process.crawl('iosco_urls', start_urls=rows)
    process.start() # the script will block here until the crawling is finished

run_url_from_regulator_scrapers()
