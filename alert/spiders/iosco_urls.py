import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Spider
from scrapy.http import Request
from ..items import IoscoAdvancedItem


USA_SEC = 'Securities and Exchange Commission'
BC_BCSC = 'British Columbia Securities Commission'
HK_SFC = 'Securities and Futures Commission'

regulator_domains = {
    USA_SEC: 'sec.gov',
    # BC_BCSC: 'bcsc.bc.ca',
    # HK_SFC: 'sfc.hk', there is some 301 loop problem
}

allowed_domains = [item for item in regulator_domains.values()]


class IoscoUrls(Spider):
    name = 'iosco_urls'
    allowed_domains = allowed_domains

    start_urls = []
    current_row = 0

    custom_settings = {
        'ITEM_PIPELINES': {
            # 'alert.pipelines.JsonPipeline': 300,
            'alert.pipelines.IoscoUrlsPostgresPipeline': 300,
        }
    }


    def start_requests(self):
        for row in self.start_urls:
            regulator_name = row[1]
            regulator = regulator_domains.get(regulator_name)
            if regulator:
                yield Request(
                    row[2],
                    dont_filter=True,
                    callback=self.get_callback(regulator_name),
                    cb_kwargs={'iosco_item_id': row[0]}
                )

    def get_callback(self, regulator_name):
        regulator_callbacks = {
            USA_SEC: self.parse_usa_sec,
            BC_BCSC: self.parse_bc_bcsc,
            HK_SFC: self.parse_hk_sfc,
        }
        return regulator_callbacks[regulator_name]

    def parse_usa_sec(self, response, iosco_item_id):
        _list = response.xpath('//*[@id="block-secgov-content"]/article/div[1]/div[2]/div[1]/div/p[2]/text()').extract()
        _list = [item for item in _list if 'website' in item.lower()]
        if (len(_list) > 0):
            urls = _list[0].lower().replace('website.', '').replace('website:', '').replace('websites:', '').strip()
            url_list = urls.split(',')

            for link in url_list:
                if link:
                    yield IoscoAdvancedItem(
                        iosco_item_id = iosco_item_id,
                        url = link
                    )

    def parse_bc_bcsc(self, response, iosco_item_id):
        link = (response.xpath('/html/body/main/div[3]/div/div[2]/div[2]/p[1]/text()').extract_first() or '').strip()
        if link:
            yield IoscoAdvancedItem(
                iosco_item_id = iosco_item_id,
                url = link
            )

    def parse_hk_sfc(self, response, iosco_item_id):
        link = (response.xpath('/html/body/div[3]/div/div/div[2]/table/tbody/tr[4]/td[2]/text()').extract_first() or '').strip()
        if link:
            yield IoscoAdvancedItem(
                iosco_item_id = iosco_item_id,
                url = link
            )
