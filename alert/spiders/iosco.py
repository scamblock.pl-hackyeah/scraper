import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import Spider
from ..items import IoscoItem


class IoscoSpider(Spider):
    name = 'iosco'
    allowed_domains = ['iosco.org']
    start_urls = ['https://www.iosco.org/investor_protection/?subsection=investor_alerts_portal']

    custom_settings = {
        'ITEM_PIPELINES': {
            # 'alert.pipelines.JsonPipeline': 300
            'alert.pipelines.IoscoPostgresPipeline': 300,
        }
    }

    api_url = 'https://www.iosco.org/investor_protection/investor_alerts/_investor_alerts.cfm?Entity=&RegulatorID=&JurisdictionID=&DateStart=&DateEnd=&Keywords=&SubjectID=&OrderBy=DatePosted&Dir=desc'


    start_page = 1
    end_page = None # set None to ommit
    # 574 sites is max for 20 row pages (on 27.11.2020)

    rows_per_page = 20
    should_run_next_loop = True

    def parse(self, response):
        while (self.should_run_next_loop):
            self.logger.info('===== PAGE [%s] =====', self.start_page)
            yield scrapy.FormRequest(
                url=self.api_url,
                formdata={'Rows': f'{self.rows_per_page}', 'CurrentPage': str(self.start_page)},
                callback=self.after_post
            )

    def after_post(self, response):
        rows = response.css('tbody')[0].css('tr')
        self.start_page += 1
        self.should_run_next_loop = (not self.end_page or self.start_page <= self.end_page) and len(rows) >= self.rows_per_page
        for row in rows:
            yield IoscoItem(
                company=(row.xpath(f'td[{1}]/text()').extract_first() or '').strip(),
                regulator=(row.xpath(f'td[{2}]/text()').extract_first() or '').strip(),
                regulator_url=(row.xpath(f'td[{4}]/a/@href').extract_first() or '').strip(),
                jurisdiction=(row.xpath(f'td[{3}]/text()').extract_first() or '').strip(),
                date=(row.xpath(f'td[{4}]/span/text()').extract_first() or '').strip(),
                subject=(row.xpath(f'td[{5}]/text()').extract_first() or '').strip(),
                comments=(row.xpath(f'td[{6}]/text()').extract_first() or '').strip()
            )
