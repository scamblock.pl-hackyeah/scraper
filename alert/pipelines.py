# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import json
import os
from collections import namedtuple

import boto3

import psycopg2
from itemadapter import ItemAdapter

sqs = boto3.client('sqs')

IoscoItem = namedtuple('IoscoItem', ['id', 'regulator', 'regulator_url'])


class JsonPipeline:
    def open_spider(self, spider):
        self.file = open('items.json', 'w')

    def close_spider(self, spider):
        self.file.close()

    def process_item(self, item, spider):
        line = json.dumps(ItemAdapter(item).asdict()) + "\n"
        self.file.write(line)
        return item
        # spider_map = {
        # 'IoscoSpider': self.iosco_item
        # }
        # func = spider_map[spider.__class__.__name__]
        # return func(item)

    # def iosco_item(self, item):
        # return item


class IoscoPostgresPipeline:
    def open_spider(self, spider):
        self.connection = create_connection()
        self.cur = self.connection.cursor()

    def close_spider(self, spider):
        self.cur.close()
        self.connection.close()

    def process_item(self, item, spider):
        self.cur.execute(
            'INSERT INTO "ioscoItems"'
            ' (company, regulator, regulator_url, jurisdiction, date, subject, comments)'
            ' VALUES(%s, %s, %s, %s, %s, %s, %s) '
            ' ON CONFLICT ("company") '
            ' DO NOTHING', (
                item['company'],
                item['regulator'],
                item['regulator_url'],
                item['jurisdiction'],
                item['date'],
                item['subject'],
                item['comments'],
            )
        )
        self.connection.commit()
        return item


class IoscoUrlsPostgresPipeline:
    def open_spider(self, spider):
        hostname = os.environ.get('DB_HOST')
        username = os.environ.get('DB_USER')
        password = os.environ.get('DB_PASSWORD')
        database = os.environ.get('DB_DATABASE')
        self.connection = psycopg2.connect(
            host=hostname, user=username, password=password, dbname=database)
        self.cur = self.connection.cursor()

    def close_spider(self, spider):
        self.cur.close()
        self.connection.close()

    def process_item(self, item, spider):
        self.cur.execute(
            'INSERT INTO "urls"'
            ' ("ioscoItemId", "url") '
            ' VALUES(%s, %s)'
            ' RETURNING id', (
                item['iosco_item_id'],
                item['url']
            ),
        )
        url_id = self.cur.fetchone()[0]
        self.connection.commit()
        trigger_checking_url(url_id)
        return item


def trigger_checking_url(url_id):
    queue_url = os.environ.get('URL_SQS_QUEUE')
    response = sqs.send_message(
        QueueUrl=queue_url,
        MessageBody=(json.dumps({'id': url_id}))
    )
