CREATE TABLE IF NOT EXISTS "ioscoItems" (
    "id" SERIAL NOT NULL,
    "company" CHARACTER VARYING NOT NULL,
    "regulator" CHARACTER VARYING NOT NULL,
    "regulator_url" CHARACTER VARYING NOT NULL,
    "jurisdiction" CHARACTER VARYING NOT NULL,
    "date" CHARACTER VARYING NOT NULL,
    "subject" CHARACTER VARYING DEFAULT '',
    "comments" CHARACTER VARYING DEFAULT '',
    "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
    "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
    CONSTRAINT "UQ_company" UNIQUE ("company")
);

ALTER TABLE "ioscoItems" ADD CONSTRAINT  "pk_ioscoItems" PRIMARY KEY (id);

CREATE TABLE IF NOT EXISTS "alerts" (
    "id" SERIAL NOT NULL,
    "ioscoItemId" INTEGER NOT NULL,
    "origin"  CHARACTER VARYING DEFAULT '',
    "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
    "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
    "points" INTEGER NOT NULL,
    CONSTRAINT "FK_ioscoItems" FOREIGN KEY ("ioscoItemId") REFERENCES "ioscoItems"("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT "pk_alerts" PRIMARY KEY (id)
);


CREATE VIEW "ioscoItemsWithPoints" AS
  SELECT
    "ioscoItems".id as "id",
    "ioscoItems".company as "company",
    "ioscoItems".regulator as "regulator",
    "ioscoItems".jurisdiction as "jurisdiction",
    "ioscoItems"."date" as "date",
    "ioscoItems"."subject" as "subject",
    "ioscoItems"."comments" as "comments",
    "ioscoItems"."createdAt" as "createdAt",
    "ioscoItems"."updatedAt" as "updatedAt",
    COALESCE(SUM("alerts"."points"), 0) AS "points"

  FROM "ioscoItems" "ioscoItems"
    LEFT JOIN "alerts" "alerts" ON "alerts"."ioscoItemId" = "ioscoItems"."id"
  GROUP BY "ioscoItems"."id";


CREATE TABLE IF NOT EXISTS "urls" (
    "id" SERIAL NOT NULL,
    "ioscoItemId" INTEGER NOT NULL,
    "url" CHARACTER VARYING NOT NULL,
    "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
    "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
    CONSTRAINT "FK_ioscoItems" FOREIGN KEY ("ioscoItemId") REFERENCES "ioscoItems"("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT "pk_urls" PRIMARY KEY (id)
);


CREATE TABLE IF NOT EXISTS "addresses" (
    "id" SERIAL NOT NULL,
    "ioscoItemId" INTEGER NOT NULL,
    "address" CHARACTER VARYING NOT NULL,
    CONSTRAINT "FK_ioscoItems" FOREIGN KEY ("ioscoItemId") REFERENCES "ioscoItems"("id") ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT "pk_addresses" PRIMARY KEY (id)
);
