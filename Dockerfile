FROM python:3.8-slim-buster
RUN apt-get update && apt-get install -y \
    gcc libpq-dev

COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app
CMD scrapy crawl iosco